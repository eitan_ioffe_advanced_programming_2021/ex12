#include "threads.h"

std::mutex mtx;

bool isPrime(int n, int i)
{
	if (n <= 1)
		return false;
	if (i * i > n) { // until sqrt of the number
		return true;
	}
	if (n % i == 0) {
		return false;
	}

	return isPrime(n, i + 1);
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	// writing prime numbers in the file
	for (int i = begin; i <= end; i++) {
		if (isPrime(i, 2)) {
			mtx.lock(); // locking - writing to file
			file << std::to_string(i) << std::endl;
			mtx.unlock();
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int range = (end - begin) / N; // the range of numbers each thread will work with

	std::vector<std::thread> thVec;
	std::ofstream file(filePath);
	if (file.is_open())
	{
		const auto before = std::chrono::system_clock::now(); // starting the timer

		for (int i = 0; i < N; i++) {	// pushing threads to vector
			begin += range; // each thread gets a different range to work with
			std::thread t(writePrimesToFile, begin - range, begin, std::ref(file));
			thVec.push_back(std::move(t));
		}

		for (int i = 0; i < N; i++) { // waiting until all threads are finished
			thVec[i].join();
		}

		const std::chrono::duration<double> duration = std::chrono::system_clock::now() - before; // calculating time
		std::cout << "The calculation took: " << duration.count() << " seconds" << std::endl;
	}
	else std::cout << "Unable to open file";
}
