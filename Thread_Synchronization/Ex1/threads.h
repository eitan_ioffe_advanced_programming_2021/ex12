#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <mutex>

bool isPrime(int n, int i); // returning if the number is prime

void writePrimesToFile(int begin, int end, std::ofstream& file); // writing all prime numbers in the range given into the file
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N); // calling N number of writePrimesToFile threads
