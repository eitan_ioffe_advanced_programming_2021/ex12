#pragma once
#include <condition_variable>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <queue>
#include <mutex>
#include <set>

#define SIGN_IN 1
#define SIGN_OUT 2
#define CONNECTED_USERS 3
#define EXIT 4

#define SECONDS_TO_WAIT 60

#define DATA_FILE "data.txt"
#define OUTPUT_FILE "output.txt"

using std::string;

class MessagesSender {
public:
	MessagesSender();
	~MessagesSender();

	void readFileData(); // reading file data and adding each line's data to queue
	void sendMessagesToUsers(); // reading data from queue and sending it to users (output file)

	void menu(); // the main menu

private:
	void printMenu(int& choice); // printing menu

	void signIn(); // signin a new user
	void signOut(); // signout existing user
	void connectedUsers(); // printing all connected users
	void checkInput(int& num, const int min, const int max); // checking that the input is valid

	std::set<string> _users;
	std::queue<string> _messages;
	std::mutex _userMtx;
	std::mutex _messageMtx;
	std::condition_variable cv;
	bool ready;
};