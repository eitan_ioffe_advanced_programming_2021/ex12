#include "MessagesSender.h"

MessagesSender::MessagesSender()
{
	ready = false;
}

MessagesSender::~MessagesSender()
{
	_users.clear();
}

void MessagesSender::menu()
{
	int choice = 1;

	while (choice != EXIT)
	{
		printMenu(choice);

		switch (choice)
		{
			case SIGN_IN:
				signIn();
				break;
			case SIGN_OUT:
				signOut();
				break;
			case CONNECTED_USERS:
				connectedUsers();
				break;
		}
	}
}

void MessagesSender::printMenu(int& choice)
{
	std::cout << "1. Signin" << std::endl;
	std::cout << "2. Signout" << std::endl;
	std::cout << "3. Connected Users" << std::endl;
	std::cout << "4. Exit" << std::endl;

	std::cin >> choice;
	(void)getchar(); // cleaning buffer
	checkInput(choice, SIGN_IN, EXIT);
}

void MessagesSender::signIn()
{
	std::string name = "";
	std::cout << "Enter a new user name: ";
	std::getline(std::cin, name);

	std::lock_guard<std::mutex> lck(_userMtx); // will unlock after the condition is completed
	if (_users.find(name) == _users.end()) // if user doesn't exist
	{ // adding the user
		_users.insert(name);
	}
	else {
		std::cerr << "User already exists" << std::endl;
	}
}

void MessagesSender::signOut()
{
	std::string name = "";
	std::cout << "Enter user name to remove: ";
	std::getline(std::cin, name);

	std::lock_guard<std::mutex> lck(_userMtx); // will unlock after the condition is completed
	if (_users.find(name) != _users.end()) // if user doesn't exist
	{ // adding the user
		_users.erase(name);
	}
	else {
		std::cerr << "User doesn't exist" << std::endl;
	}
}

void MessagesSender::connectedUsers()
{
	std::unique_lock<std::mutex> lck(_userMtx); // locking users mutex
	for (std::set<string>::iterator it = _users.begin(); it != _users.end(); ++it) { // printing users
		std::cout << (*it) << std::endl;
	}
	lck.unlock(); // unlocking back
}

void MessagesSender::checkInput(int& num, const int min, const int max)
{
	while (num < min || num > max) {
		std::cout << "Wrong choice, enter again:" << std::endl;
		std::cin >> num;
		(void)getchar(); // clean buffer
	}
}

void MessagesSender::readFileData()
{
	std::fstream myfile;
	string buffer = "";

	while (true)
	{
		myfile.open(DATA_FILE); // opening file for reading

		if (myfile.is_open()) {
			while (std::getline(myfile, buffer)) // pushing each line to queue
			{
				std::lock_guard<std::mutex> lck(_messageMtx); // locking message mutex
				_messages.push(buffer);
				ready = true;
			}
			if (ready) // notifying to other thread to start sending the messages
				cv.notify_all();

			myfile.close();

			// wiping file data
			myfile.open(DATA_FILE, std::fstream::out | std::fstream::trunc);
			myfile.close();
		}

		// waiting 60 seconds
		std::chrono::seconds dura(SECONDS_TO_WAIT);
		std::this_thread::sleep_for(dura);
	}
}

void MessagesSender::sendMessagesToUsers()
{
	std::set<string>::iterator it;
	std::fstream file;
	string msg = "", finaleMsg = "";

	// wiping file data
	file.open(OUTPUT_FILE, std::fstream::out | std::fstream::trunc);
	file.close();

	while (true)
	{
		file.open(OUTPUT_FILE, std::fstream::app); // opening file for writing (without overwriting)

		// waiting until new messages appear in queue
		std::unique_lock<std::mutex> lckMsg(_messageMtx);
		if (!ready) cv.wait(lckMsg);
		ready = false;
		lckMsg.unlock();

		lckMsg.lock(); // locking msg because we check if it is empty
		while (!_messages.empty())
		{
			msg = _messages.front();
			_messages.pop();

			lckMsg.unlock(); // unlocking msg - not using right now
			std::lock_guard<std::mutex> lckUser(_userMtx);
			for (it = _users.begin(); it != _users.end(); ++it) { // sending messages to connected users
				finaleMsg = (*it) + ": " + msg + "\n";
				file << finaleMsg;
			}
			lckMsg.lock(); // locking msg back
		}

		lckMsg.unlock();
		file.close();
	}
}
