#include "MessagesSender.h"

int main()
{
	MessagesSender msg;

	// calling 2 threads to read messages from file and send them the all connected users
	std::thread t1(&MessagesSender::readFileData, &msg);
	std::thread t2(&MessagesSender::sendMessagesToUsers, &msg);
	t1.detach();
	t2.detach();

	msg.menu();

	return 0;
}